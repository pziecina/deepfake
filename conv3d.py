import pickle
import tensorflow as tf
import numpy as np
import h5py
from keras.layers import Input
from keras.models import Model
from keras.models import load_model
from keras.layers.convolutional import Convolution3D as Conv3d
from keras.layers.convolutional import Convolution2D as Conv2d
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import MaxPooling3D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers import GlobalAveragePooling2D
from keras.layers import GlobalMaxPooling1D
from keras.layers import Concatenate
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Add
from keras.layers import Dropout
from keras.callbacks import ModelCheckpoint
from sklearn.utils import class_weight
import matplotlib.pyplot as plt
import efficientnet.keras as efn
from sklearn.metrics import confusion_matrix

from tensorflow.keras.utils import plot_model

# tf.device('/CPU:0')


FRAMES_NUMBER = 1
IMG_SIZE = 224

CHUNK_NR = 0
CNNFOLDER = "CNN3D/"
MODELNAME = "my_model" + str(IMG_SIZE) + "_" + str(FRAMES_NUMBER) + "/"
WEIGHTS = CNNFOLDER + MODELNAME
print(WEIGHTS)

# deepfake  / photoshop


# print("Num GPUs Available: ", tf.config.experimental.list_physical_devices('GPU'))


def openData():
    with open("pickle3D/X_face_"+str(IMG_SIZE)+"_"+str(FRAMES_NUMBER)+"_chunk_"+str(CHUNK_NR)+".pickle", "rb") as pickle_in:
    # with open("pickle/photoshop/X224.pickle", "rb") as pickle_in:
        X = pickle.load(pickle_in) # / 255.0
        print(X.dtype)
        X = np.array(X) / 255.0

    with open("pickle3D/X_audio_" + str(IMG_SIZE) + "_" + str(FRAMES_NUMBER) + "_chunk_" + str(CHUNK_NR) + ".pickle", "rb") as pickle_in:
        # with open("pickle/photoshop/X224.pickle", "rb") as pickle_in:
        X_AUDIO = pickle.load(pickle_in)  # / 255.0
        print(X_AUDIO.dtype)

    with open("pickle3D/y_face_"+str(IMG_SIZE)+"_"+str(FRAMES_NUMBER)+"_chunk_"+str(CHUNK_NR)+".pickle", "rb") as pickle_in:
    # with open("pickle/photoshop/y224.pickle","rb") as pickle_in:
        y = pickle.load(pickle_in)

    return X, X_AUDIO, y


X, X_AUDIO, y = openData()
X = np.squeeze(X)
# print(X)
print(X_AUDIO.shape)
X_AUDIO = np.array(X_AUDIO).reshape(-1, 12, 432, 1)
print("shape", X_AUDIO.shape)
# print(X.dtype)
yprim = y
print(yprim.shape)
# print(np.sum(yprim))
# print("OD 601: ",np.sum(yprim[601:700]))
# i = 0
# XSYM = []
# ysym = []
# j=0
#
# print(np.sum(y[1:800]))
#
# for i, value in enumerate(X):
#     # print(i)
#     if y[i] == 0 and j < 62:
#         XSYM.append(X[i])
#         ysym.append(y[i])
#         j+=1
#     if y[i] == 1:
#         XSYM.append(X[i])
#         ysym.append(y[i])
#
# XSYM = np.array(XSYM)
# ysym = np.array(ysym)

    # i+=1
    # print(rows)

#
#
def create_model_functional2d():
    input1 = Input(shape=(IMG_SIZE, IMG_SIZE, 3))
    input2 = Input(shape=(12, 432,1))

    x1 = Conv2d(64, (3, 3), activation=tf.nn.relu)(input2)
    x1 = Conv2d(64, (3, 3), activation=tf.nn.relu)(x1)
    x1 = BatchNormalization()(x1)
    x1 = MaxPooling2D(pool_size=(2, 2))(x1)
    x1 = Conv2d(64, (2, 8), activation=tf.nn.relu)(input2)
    x1 = Conv2d(512, (2, 8), activation=tf.nn.relu)(x1)
    x1 = BatchNormalization()(x1)
    x1 = MaxPooling2D(pool_size=(1, 40))(x1)

    x = Conv2d(64, (3, 3), activation=tf.nn.relu)(input1)
    x = Conv2d(64, (3, 3), activation=tf.nn.relu)(x)
    x = BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2d(128, (3, 3), activation=tf.nn.relu)(x)
    x = Conv2d(128, (3, 3), activation=tf.nn.relu)(x)
    x = BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2d(256, (3, 3), activation=tf.nn.relu)(x)
    x = Conv2d(256, (3, 3), activation=tf.nn.relu)(x)
    x = BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Conv2d(512, (3, 3), activation=tf.nn.relu)(x)
    x = Conv2d(512, (3, 3), activation=tf.nn.relu)(x)
    x = BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)

    o = Concatenate()([x,x1])

    o = Flatten()(o)
    # x = Dense(4096, activation=tf.nn.relu)(x)
    # x = Dropout(0.5)(x)
    # x = Dense(4096, activation=tf.nn.relu)(x)
    # x = Dropout(0.5)(x)
    # x = Dense(1024, activation=tf.nn.relu)(x)
    # x = Dropout(0.5)(x)
    output = Dense(2, activation=tf.nn.sigmoid)(o)

    model = Model(inputs=[input1, input2], outputs=output)

    model.summary()

    return model


drop_out_rate = 0.5
def create_efficientnet_with_audio():
    model = efn.EfficientNetB0(weights='imagenet')

# AUDIO MODEL
    inputAudio = Input(shape=(12, 432, 1))

    x1 = Conv2d(8, (1,7), padding='valid', activation=tf.nn.relu, strides=1)(inputAudio)
    x1 = MaxPooling2D(1, 2)(x1)
    x1 = Dropout(drop_out_rate)(x1)
    x1 = Conv2d(16, (1,5), padding='valid', activation=tf.nn.relu, strides=1)(x1)
    x1 = MaxPooling2D(1, 2)(x1)
    x1 = Dropout(drop_out_rate)(x1)
    x1 = Conv2d(32, (1,5), padding='valid', activation=tf.nn.relu, strides=1)(x1)
    x1 = MaxPooling2D(1, 2)(x1)
    x1 = Dropout(drop_out_rate)(x1)
    x1 = Conv2d(64, (1,5), padding='valid', activation=tf.nn.relu, strides=1)(x1)
    x1 = MaxPooling2D(1, 2)(x1)
    x1 = Dropout(drop_out_rate)(x1)
    x1 = Conv2d(128, (1,3), padding='valid', activation=tf.nn.relu, strides=1)(x1)
    x1 = MaxPooling2D(1, 2)(x1)
    x1 = Flatten()(x1)
    x1 = Dense(256, activation='relu')(x1)
    x1 = Dropout(drop_out_rate)(x1)
    x1 = Dense(128, activation='relu')(x1)
    x1 = Dropout(drop_out_rate)(x1)


    # x1 = Conv2d(64, (2, 3), activation=tf.nn.relu)(x1)
    # x1 = BatchNormalization()(x1)
    # x1 = MaxPooling2D(pool_size=(1, 2))(x1)
    #
    # x1 = Conv2d(128, (1, 3), activation=tf.nn.relu)(x1)
    # x1 = Conv2d(128, (1, 3), activation=tf.nn.relu)(x1)
    # x1 = BatchNormalization()(x1)
    # x1 = MaxPooling2D(pool_size=(1, 2))(x1)


######

    model.layers.pop()
    x = model.layers[-1].output

    # model.summary()

    o = Concatenate()([x, x1])
    o = Dense(256, activation=tf.nn.sigmoid)(o)
    o = Dropout(drop_out_rate)(o)
    o = Dense(128, activation=tf.nn.sigmoid)(o)
    o = Dropout(drop_out_rate)(o)
    output = Dense(2, activation=tf.nn.sigmoid)(o)

    model1 = Model(inputs=[model.input, inputAudio], outputs=output)

    # model1.summary()

    a=0
    for layer in model1.layers:
       a+=1

    print("layers",a)
    # for layer in model1.layers[:100]:
    #     layer.trainable = False

    return model1

def create_efficientnet():
    model = efn.EfficientNetB0(weights='imagenet')

    model.layers.pop()
    x = model.layers[-1].output
    output = Dense(2, activation=tf.nn.sigmoid)(x)

    model1 = Model(inputs=model.input, outputs=output)

    a=0
    for layer in model1.layers:
       a+=1

    print("layers",a)
    # for layer in model1.layers[:100]:
    #     layer.trainable = False

    return model1


def create_model_functional():
    input1 = Input(shape=(FRAMES_NUMBER, IMG_SIZE, IMG_SIZE, 3))

    x = Conv3d(64, (2, 3, 3), activation=tf.nn.relu)(input1)
    x = Conv3d(64, (2, 3, 3), activation=tf.nn.relu)(x)
    x = BatchNormalization()(x)
    x = MaxPooling3D(pool_size=(1, 2, 2))(x)

    x = Conv3d(128, (1, 3, 3), activation=tf.nn.relu)(x)
    x = Conv3d(128, (1, 3, 3), activation=tf.nn.relu)(x)
    x = BatchNormalization()(x)
    x = MaxPooling3D(pool_size=(1, 2, 2))(x)

    x = Conv3d(256, (1, 3, 3), activation=tf.nn.relu)(x)
    x = Conv3d(256, (1, 3, 3), activation=tf.nn.relu)(x)
    x = BatchNormalization()(x)
    x = MaxPooling3D(pool_size=(1, 2, 2))(x)

    x = Conv3d(512, (1, 3, 3), activation=tf.nn.relu)(x)
    x = Conv3d(512, (1, 3, 3), activation=tf.nn.relu)(x)
    x = BatchNormalization()(x)
    x = MaxPooling3D(pool_size=(1, 2, 2))(x)

    x = Flatten()(x)
    # x = Dense(4096, activation=tf.nn.relu)(x)
    # x = Dropout(0.5)(x)
    # x = Dense(4096, activation=tf.nn.relu)(x)
    # x = Dropout(0.5)(x)
    # x = Dense(1024, activation=tf.nn.relu)(x)
    # x = Dropout(0.5)(x)
    output = Dense(1, activation=tf.nn.sigmoid)(x)

    model = Model(inputs=input1, outputs=output)

    model.summary()

    return model


def load_trained_data(weights_path):
    model = create_model_functional()
    model.load_weights(weights_path)
#
#


def train():
    checkpoint = ModelCheckpoint("EFN_2D_224.hdf5", monitor='loss', verbose=1,
                                 save_best_only=True, mode='auto', period=1)
    model = create_efficientnet()

    # def scheduler(epoch):
    #     if epoch < 10:
    #         return 0.001
    #     else:
    #         return 0.001 * tf.math.exp(0.1 * (10 - epoch))
    #
    # callback = tf.keras.callbacks.LearningRateScheduler(scheduler)
    model.compile(optimizer=tf.keras.optimizers.SGD(lr=0.01),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    history = model.fit([X, X_AUDIO], yprim, epochs=300, validation_split=0.2, class_weight='auto', callbacks=[checkpoint])
    # model.save_weights(WEIGHTS)
    return model, history
#
#
# #
#
# print(X,y)
model_new, history = train()
model_new.summary()
#
# # #
# # # # #
# checkpoint = ModelCheckpoint("EFN_AUDIO_224.hdf5", monitor='loss', verbose=1,
#                                  save_best_only=True, mode='auto', period=1)
# model = create_efficientnet()
# model.load_weights("EFN_AUDIO_224.hdf5")
# model.compile(optimizer=tf.keras.optimizers.SGD(lr=0.001),
#                   loss='sparse_categorical_crossentropy',
#                   metrics=['accuracy'])
# history = model.fit([X, X_AUDIO], yprim, batch_size=32, epochs=100, validation_split=0.2, class_weight='auto', callbacks=[checkpoint])
# # # # # #
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

#
#
# # model_new.summary()
# # # #

#
# model_new = create_model_functional2d()
# model_new.load_weights("SYM3D_224_2D.hdf5")
# model_new.summary()
# Y = model_new.predict([X,X_AUDIO])
# Y_argmax = (Y[:, 0] < Y[:, 1]).astype(np.int)
# print(confusion_matrix(yprim, Y_argmax))


# model_new.summary()
# # # model_new.load_weights(WEIGHTS)
# #
# plot_model(model_new, to_file='graphs/model.png',
#                    show_shapes=True, expand_nested=True)
# #
# # # model_new.evaluate(X[701:742], yprim[701:742])

# # print(Y)
# print(yprim[701:742])
# # # print(yprim[701:742])
# Y_map = list(map(max, Y))
# #
# # # print(Y[1:10])
# # # print(Y_map[1:10])
# print(Y_argmax[0:42])
# # #
# # # print(yprim[1:100])
#
# print(np.average(Y_argmax[0:42] == yprim[701:742]))

# model_new.summary()