import cv2
import os
import numpy as np
from tqdm import tqdm
from matplotlib import pyplot as plt

# img = cv2.imread('C:/Users/lykan/Desktop/deepfake/easy_2_1111.jpg', 0)
# f = np.fft.fft2(img)
# fshift = np.fft.fftshift(f)
# magnitude_spectrum = 20*np.log(np.abs(fshift))
#
# plt.subplot(121), plt.imshow(img, cmap='gray')
# plt.title('Input Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(122), plt.imshow(magnitude_spectrum, cmap='gray')
# plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
# plt.show()


DATADIR = "C:/Users/lykan/Desktop/deepfake"
# CATEGORIES = ["1.TruePhotoshop"]
CATEGORIES = ["2.FakeDeep"]

training_data = []


def calculate_fft(img_array):
    mask = create_mask(img_array)
    print(np.max(np.float32(img_array)),np.min(np.float32(img_array)))
    dft = cv2.dft(np.float32(img_array), flags=cv2.DFT_COMPLEX_OUTPUT)

    # FFT IMAGE
    fshift = np.fft.fftshift(dft)
    magnitude_spectrum = cv2.magnitude(fshift[:, :, 0], fshift[:, :, 1])
    print(np.max(magnitude_spectrum), np.min(magnitude_spectrum))
    mgs_log = 20 * np.log(magnitude_spectrum)                           #TO DISPLAY FFT IN LOG SCALE

    # FFT IMAGE WITH MASK
    print(np.min(fshift), np.max(fshift))
    fshift2 = fshift * mask
    magnitude_spectrum_mask = 20 * np.log(cv2.magnitude(fshift2[:, :, 0], fshift2[:, :, 1]))  #TO DISPLAY FFT IN LOG SCALE WITH MASK

    # INVERS FFT
    # print(np.max(magnitude_spectrum), np.min(magnitude_spectrum))
    # print(np.max(magnitude_spectrum_mask), np.min(magnitude_spectrum_mask))
    # print(np.max(mask), np.min(mask))
    # print(magnitude_spectrum.shape)
    # fshift = np.exp(magnitude_spectrum/20)

    f_ishift = np.fft.ifftshift(fshift2)
    # print(f_ishift.shape)
    img_back = cv2.idft(f_ishift)
    # print(img_array, np.abs(img_back))
    # print(np.max(img_back), np.min(img_back))
    # print(img_back.shape)
    img_back = cv2.magnitude(img_back[:, :, 0], img_back[:, :, 1])
    # print(np.max(img_array - img_back), np.min(img_array - img_back))
    min, max = np.amin(img_back, (0, 1)), np.amax(img_back, (0, 1))
    print(min, max)
    img_back = cv2.normalize(img_back, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

    dft2 = cv2.dft(np.float32(img_back), flags=cv2.DFT_COMPLEX_OUTPUT)
    fshift3 = np.fft.fftshift(dft2)
    magnitude_spectrum2 = cv2.magnitude(fshift3[:, :, 0], fshift3[:, :, 1])
    mgs_log2 = 20 * np.log(magnitude_spectrum2)
    # print(img_array,np.abs(img_back))
    # print(np.max(img_back), np.min(img_back))


    return mgs_log, magnitude_spectrum_mask, img_back, mgs_log2

def create_mask(img_array):
    # Circular HPF mask, center circle is 0, remaining all ones
    rows, cols = img_array.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.zeros((rows, cols, 2), np.uint8)
    # print(mask)
    r = 10
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r * r
    # mask_1 = (x >= 500) & (x <= 520) & (y > 500) & (y < 524)  #CENTER
    # mask_1 = (x >= 0) & (x <= crow*0.98) & (y > ccol*0.96) & (y < ccol*1.04)  #LEFT_LINE
    # mask_2 = (x >=crow*1.02) & (x < crow*2) & (y > ccol*0.96) & (y < ccol*1.04) #RIGHT_LINE
    mask_1 = (x >= 0) & (x <= crow*0.98) & (y > ccol*0.995) & (y < ccol*1.005)  #LEFT_LINE_SMALL_ON_|
    mask_2 = (x >=crow*1.02) & (x < crow*2) & (y > ccol*0.995) & (y < ccol*1.005) #RIGHT_LINE_SMALL_ON |


    # mask[mask_area] = 0
    mask[mask_1] = 1
    mask[mask_2] = 1

    return mask

def create_img_with_mask():
    for category in CATEGORIES:
        path = os.path.join(DATADIR, category)
        path2 = os.path.join(DATADIR, "2.FakeDeepFFT")
        path3 = os.path.join(DATADIR, "2.FakeDeepIFFT")
        # class_num = CATEGORIES.index(category)
        for img in tqdm(os.listdir(path)[1:100]):
            if img.endswith('.jpg'):
                img_array = cv2.imread(os.path.join(path, img), 0)  #add colors 3 surface
                # print(img_array.dtype)

                mag_spec, mag_spec_filter, img_back, mgs = calculate_fft(img_array)

                # cv2.imwrite(os.path.join(path2, os.path.basename(img)), fshift_mask_mag)
                plt.subplot(2, 3, 1), plt.imshow(img_array, cmap='gray')
                plt.title('Input Image')
                plt.subplot(2, 3, 2), plt.imshow(mag_spec, cmap='gray')
                plt.title('After FFT')
                plt.subplot(2, 3, 3), plt.imshow(mag_spec_filter, cmap='gray')
                plt.title('FFT + Mask')
                plt.subplot(2, 3, 4), plt.imshow(img_back, cmap='gray')
                plt.title('After FFT Inverse')
                plt.subplot(2, 3, 5), plt.imshow(img_array - img_back, cmap='gray')
                plt.title('After FFT Inverse')
                plt.subplot(2, 3, 6), plt.imshow(mgs, cmap='gray')
                plt.title('After FFT Inverse')
                plt.show()

                # np.savetxt("array.txt", img_back)

                # img_back = cv2.cvtColor(img_back, cv2.COLOR_BGR2RGB)
                # cv2.imwrite(os.path.join(path3, os.path.basename(img)), img_back.astype(np.uint8))

                # plt.subplot(121), plt.imshow(img_array)
                # plt.title('Input Image'), plt.xticks([]), plt.yticks([])
                # plt.subplot(122), plt.imshow(magnitude_spectrum)
                # plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
                # plt.show()
                # new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
                # training_data.append(magnitude_spectrum)
                # print(training_data)



if __name__ == "__main__":
    create_img_with_mask()