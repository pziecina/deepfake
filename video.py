import numpy as np
import os
import json
import csv
from tqdm import tqdm
from mtcnn import MTCNN
from scipy import signal
from matplotlib import pyplot as plt
import radialProfile as rp
import cv2

def calcHistDiff(frame1,frame2,color):
    fremp = cv2.calcHist(frame1, [color], None, [256], (0, 256), accumulate=False)
    cv2.normalize(fremp, fremp, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX)
    temp = cv2.calcHist(frame2, [color], None, [256], (0, 256), accumulate=False)
    cv2.normalize(fremp, fremp, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX)
    diff = cv2.compareHist(fremp, temp, 0)
    return diff

def calcNormCcor(frame1,frame2):
    ccornorm = cv2.matchTemplate(cropframe, tmp, cv2.TM_CCORR_NORMED)
    ccornorm2 = np.amax(ccornorm)
    return ccornorm2


# D:\DatasetDeepFakeChallenge\dfdc_train_part_01\dfdc_train_part_1
#1
# cap = cv2.VideoCapture('aayrffkzxn.mp4')
# # cap = cv2.VideoCapture('abhggqdift.mp4') #fake

#2
# cap = cv2.VideoCapture('psbaqedyql.mp4')
# cap = cv2.VideoCapture(DATADIR + "aassnaulhq.mp4") #fake

face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

DATADIR = "D:\DatasetDeepFakeChallenge"

training_data =[]

for folder in os.listdir(DATADIR):
    if folder.endswith(".zip"):
        continue
    path = os.path.join(DATADIR, folder)
    for folder2 in os.listdir(path):
        path2 = os.path.join(path, folder2)
        for file in os.listdir(path2):
            # print(file)
            if file.endswith(".mp4"):
                # print(file)
                videoPath = os.path.join(path2, file)
                cap = cv2.VideoCapture(videoPath)
                jsonFilePath = os.path.join(path2, "metadata.json")
                with open(jsonFilePath) as json_file:
                    data = json.load(json_file)
                    # print(data)
                    for record in data:
                        if(record == file):
                            label = data[record]['label']
                            print("elo")
                            # print(data[record]['label'], record)

                WYJEBACTO = 0
                tmp = []                        #Wyjebac to gdzies w sensie te zmienne tutaj
                arraycorel = []
                avg = 0
                i = 0
                j = 0
                diffHist = 0
                cropframe = 0
                std = 0
                histarray = []
                detector = MTCNN()
                while(cap.isOpened()):
                    ret, frame = cap.read()

                    if ret is False:
                        with open('average19FPS.csv', 'a', newline='') as csv_file:
                            fieldnames = ["FFT", "Hist", "CCOR", "file type", "file name"]
                            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                            dict = {'FFT': std_avg, 'Hist': diffHist, 'CCOR': ccor, 'file type': label,
                                    'file name': file}
                            writer.writerow(dict)
                            # std_avg_plus_label = str(std_avg) + " " + label + "\n"
                            # file.write(std_avg_plus_label)
                        break

                    if cap.get(cv2.CAP_PROP_POS_FRAMES) == 100:
                        if WYJEBACTO == 20 - 1:
                            with open('average19FPS.csv', 'a', newline='') as csv_file:
                                fieldnames = ["FFT", "Hist", "CCOR", "file type", "file name"]
                                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                                dict = {'FFT': std_avg, 'Hist': diffHist, 'CCOR': ccor, 'file type': label,
                                        'file name': file}
                                writer.writerow(dict)
                        break
                    result = detector.detect_faces(frame)

                    cv2.namedWindow("output", cv2.WINDOW_NORMAL)
                    imS = cv2.resize(frame, (1000, 1000))


                    if len(result) == 0:
                        frontal = face_cascade.detectMultiScale(frame, 1.1, 4)
                        if len(frontal) == 0:
                            continue

                    if len(result) != 0:
                        WYJEBACTO+=1
                        j=0
                        offset = 50
                        x, y, w, h = result[0]['box']
                        x = max(x-offset,0)
                        y = max(y-offset,0)
                        cropframe = frame[y:y + offset*2 + h, x:x + offset*2 + w]
                        cv2.rectangle(frame, (x, y), (x + offset * 2 + w, y + offset * 2 + h), (255, 255, 0), 2)
                        frame = cv2.resize(frame, (700, 700))
                        cv2.imshow('iamge', cropframe)
                        # cv2.rectangle(imS, (x, y), (x + offset*2 + w, y + offset*2), (255, 0, 0), 2)
                        cropframe = cv2.resize(cropframe, (224, 224))
                        frontal = []

                    cv2.imshow('frame', frame)
                    if len(frontal) != 0:
                        WYJEBACTO += 1
                        x, y, w, h = frontal[0]
                        offset = 50
                        x = max(x - offset, 0)
                        y = max(y - offset, 0)
                        cropframe = frame[y:y + offset * 2 + h, x:x + offset * 2 + w]
                        cv2.imshow('iamge', cropframe)
                        cropframe = cv2.resize(cropframe, (224, 224))
                        result = []

                    # cv2.imshow('iamge', cropframe)


                    # if i == 20:
                    #     with open('average19FPS.csv', 'a', newline='') as csv_file:
                    #         fieldnames = ["FFT", "Hist", "CCOR", "file type", "file name"]
                    #         writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                    #         dict = {'FFT': std_avg, 'Hist': diffHist, 'CCOR': ccor, 'file type': label, 'file name': file}
                    #         writer.writerow(dict)
                    #         # std_avg_plus_label = str(std_avg) + " " + label + "\n"
                    #         # file.write(std_avg_plus_label)
                    #     break

                    # if len(tmp) != 0:
                    #     print(len(cropframe), len(cropframe[0]), len(tmp),  len(tmp[0]))

                    if len(tmp) != 0:
                        elo2 = cv2.cvtColor(cropframe,cv2.COLOR_BGR2GRAY)
                        dft = cv2.dft(np.float32(elo2), flags=cv2.DFT_COMPLEX_OUTPUT)
                        dft_shift = np.fft.fftshift(dft)
                        magnitude_spectrum = 20 * np.log(cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1]))
                        psd1D = rp.azimuthalAverage(magnitude_spectrum)
                        elo = cv2.cvtColor(tmp, cv2.COLOR_BGR2GRAY)
                        dft2 = cv2.dft(np.float32(elo), flags=cv2.DFT_COMPLEX_OUTPUT)
                        dft_shift2 = np.fft.fftshift(dft2)
                        magnitude_spectrum2 = 20 * np.log(cv2.magnitude(dft_shift2[:, :, 0], dft_shift2[:, :, 1]))
                        psd2D = rp.azimuthalAverage(magnitude_spectrum2)
                        p = psd2D - psd1D
                        # plt.subplot(331), plt.imshow(elo2, cmap='gray')
                        # plt.title('Input Image')
                        # plt.subplot(332), plt.imshow(magnitude_spectrum, cmap='gray')
                        # plt.title('Magnitude Spectrum')
                        # plt.subplot(333), plt.semilogy(psd1D)
                        # plt.title('1d spatial')
                        # plt.subplot(334), plt.imshow(elo, cmap='gray')
                        # plt.title('Second image')
                        # plt.subplot(335), plt.imshow(magnitude_spectrum2, cmap='gray')
                        # plt.title('Second magnitude')
                        # plt.subplot(336), plt.semilogy(psd2D)
                        # plt.title('1d spatial')
                        # plt.subplot(337), plt.plot(p)
                        # plt.title('DIFF spatial')
                        # plt.show()

                        std += np.std(p)

                        i += 1
                        std_avg = std/i
                        print(std_avg, i, label, file)

                        diff0 = calcHistDiff(cropframe, tmp, 0)
                        diff1 = calcHistDiff(cropframe, tmp, 1)
                        diff2 = calcHistDiff(cropframe, tmp, 2)
                        diffHist += diff0 + diff1 + diff2
                        diff = cv2.absdiff(cropframe, tmp)
                        #
                        ccor = calcNormCcor(cropframe, tmp)
                        avg += ccor
                        #
                        # i += 1
                        #
                        # # print(ccornorm2, i)
                        # # print(avg/i)
                        # print(diffHist, i)
                        print("DIFF HIST", diffHist / i*3)

                        # if(i ==299):
                        #     print(arraycorel)
                        #     print(histarray)
                        # print(elo)
                        # cv2.imshow('frame', base_base)
                        # cv2.imshow('frame', tmp)


                    tmp = cropframe

                    if cv2.waitKey(25) & 0xFF == ord('q'):
                        break

                    # input("Press Enter to continue...")
                if i != 0:
                    print(avg/i)

                cap.release()
                cv2.destroyAllWindows()







