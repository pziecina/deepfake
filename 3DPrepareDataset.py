import os
import cv2
import numpy as np
from tqdm import tqdm
from mtcnn import MTCNN
import moviepy.editor as mp
import librosa
import random
import pickle
import face_recognition
import json
import time

DATADIR = "C:/Users/lykan/Desktop/deepfake"
CATEGORIES = ["FAKE", "REAL"]
PICKLEPATH = "pickle3D/"
CHUNK_NR_FOLDER = "01"
CHUNK_NR = 1
FOLDER = "D:/DatasetDeepFakeChallenge/dfdc_train_part_"+CHUNK_NR_FOLDER+"/dfdc_train_part_"+str(CHUNK_NR)

MC = 0
FC = 0


training_data = []
IMG_SIZE = 224
FRAMES_NUMBER = 1     # NEW
OFFSET = 50

def get_audio(path):
    clip = mp.VideoFileClip(path)
    clip.audio.write_audiofile("tmp.wav")

    y, sr = librosa.load("tmp.wav")

    chromagram = librosa.feature.chroma_cqt(y, sr=sr, hop_length=512)

    return chromagram


path = os.path.join(DATADIR, FOLDER)

jsonFilePath = os.path.join(path, "metadata.json")
with open(jsonFilePath) as json_file:
    data = json.load(json_file)

class_num = 0
detector = MTCNN()
for video in tqdm(os.listdir(path)):
    if not video.endswith(".mp4"):
        continue

    label = data[video]['label']
    class_num = CATEGORIES.index(label)

    videopath = os.path.join(path, video)

    np_audio_array = get_audio(videopath)

    cap = cv2.VideoCapture(videopath)

    i=0
    video_array = []
    np_video_array = []
    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret is False:
        #     training_data.append([np_video_array, class_num])
            break
        if cap.get(cv2.CAP_PROP_POS_FRAMES) == FRAMES_NUMBER+1:
            # print("MC: ", MC, "FC: ", FC)
            if len(np_video_array) == FRAMES_NUMBER:
                print("SAVED", len(np_video_array))
                training_data.append([np_video_array, np_audio_array,class_num])
            break
        # print(cap.get(cv2.CAP_PROP_CONVERT_RGB))
        # cv2.imread()
        # frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
        # cv2.imshow("elo",frame)

        result = detector.detect_faces(frame)

        # if(len(result) != 0):
        #     MC += 1
        #
        # face_landmarks_list = face_recognition.face_landmarks(e)
        #
        # if(len(face_landmarks_list) != 0):                                                      #DELETE FROM HERE
        #     FC += 1
        #                                                          # TO HERE

        print(label)

        if len(result) != 0:
            x, y, w, h = result[0]['box']
            print(result[0]['confidence'])
            # print("calosc", result)
            x = max(x - OFFSET, 0)
            y = max(y - OFFSET, 0)
            cropframe = frame[y:y + OFFSET * 2 + h, x:x + OFFSET * 2 + w]
            new_array = cv2.resize(cropframe, (IMG_SIZE, IMG_SIZE))
            video_array.append(new_array)
            np_video_array = np.array(video_array)
            # print("DLUGOSC", len(np_video_array))
            result = 0

        cv2.imshow("FACE", new_array)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


    print(np.asarray(training_data).shape)
    cap.release()
    cv2.destroyAllWindows()


print((len(training_data)))

true_faces = np.copy(training_data)

for _ in range(0, 18):
    for features, audio, label in true_faces:
        if label == 1:
            training_data.append([features, audio, label])


random.shuffle(training_data)

X = []
X_AUDIO = []
y = []

for features, audio, label in training_data:
    X.append(features)
    X_AUDIO.append(audio)
    y.append([label])

# print(X.shape)
# print(X[0].reshape(-1, IMG_SIZE, IMG_SIZE, 3))
print("LICZBA TRUE", np.sum(y))

# X = np.array(X).reshape(-1, IMG_SIZE, IMG_SIZE, 3)  #add colors 3 surface
X = np.array(X)
print(X.shape)

X_AUDIO = np.array(X_AUDIO)
print(X_AUDIO.shape)

print(y)
y = np.array(y)
print(y.shape)
# #
pickle_out = open(PICKLEPATH + "X_face_"+str(IMG_SIZE)+"_"+str(FRAMES_NUMBER)+"_chunk_"+str(CHUNK_NR)+".pickle", "wb")
pickle.dump(X, pickle_out)
pickle_out.close()
#
pickle_out = open(PICKLEPATH + "X_audio_"+str(IMG_SIZE)+"_"+str(FRAMES_NUMBER)+"_chunk_"+str(CHUNK_NR)+".pickle", "wb")
pickle.dump(X_AUDIO, pickle_out)
pickle_out.close()
#
pickle_out = open(PICKLEPATH + "y_face_"+str(IMG_SIZE)+"_"+str(FRAMES_NUMBER)+"_chunk_"+str(CHUNK_NR)+".pickle", "wb")
pickle.dump(y, pickle_out)
pickle_out.close()