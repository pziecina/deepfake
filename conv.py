import pickle
import tensorflow as tf
import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.layers import GlobalMaxPooling2D
from keras.models import Model
from keras.layers import Dense
from keras.callbacks import ModelCheckpoint
import efficientnet.keras as efn
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

CNNFOLDER = "CNN/"
MODELNAME = "my_model224/"
WEIGHTS = CNNFOLDER + MODELNAME

# deepfake  / photoshop

pickle_in = open("pickle/TestoweFoty/Test.pickle", "rb")
X = pickle.load(pickle_in) / 255.0

pickle_in = open("pickle/photoshop/y224_Photoshop.pickle", "rb")
y = pickle.load(pickle_in)

print(X.shape)

yprim = np.array(y, dtype="uint8")


def create_model():
    model = tf.keras.models.Sequential([
        tf.keras.layers.Conv2D(32, (3, 3), activation=tf.nn.relu, input_shape=(224, 224, 1)),
        tf.keras.layers.Conv2D(32, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),

        tf.keras.layers.Conv2D(64, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.Conv2D(64, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),

        tf.keras.layers.Conv2D(128, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.Conv2D(128, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),

        tf.keras.layers.Conv2D(256, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.Conv2D(256, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),

        tf.keras.layers.Conv2D(512, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.Conv2D(512, (3, 3), activation=tf.nn.relu),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),

        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(4096, activation=tf.nn.relu),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(2048, activation=tf.nn.relu),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(1024, activation=tf.nn.relu),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(2, activation=tf.nn.softmax)
    ])
    return model


def create_efficientnet():
    model = efn.EfficientNetB0(weights='imagenet')

    model.trainable = True

    model.layers.pop()
    x = model.layers[-1].output
    output = Dense(2, activation="sigmoid")(x)

    model1 = Model(inputs=model.input, outputs=output)

    # for layer in model1.layers[:100]:
    #     layer.trainable = False

    return model1


def load_trained_data(weights_path):
    model = create_model()
    model.load_weights(weights_path)


def train():
    checkpoint = ModelCheckpoint("EFN_DEEPGAN_224.hdf5", monitor='loss', verbose=1,
                                 save_best_only=True, mode='auto', period=1)
    model = create_efficientnet()
    model.compile(optimizer=tf.keras.optimizers.Adam(lr=0.001),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    history = model.fit(X[1:1500], yprim[1:1500], epochs=30, validation_split=0.2, class_weight='auto'
                        , callbacks=[checkpoint])
    # model.save_weights(WEIGHTS)
    return model, history


#

# print(X,y)
# model_new, history = train()
# model_new.summary()
# #
#
# #
# checkpoint = ModelCheckpoint("EFN_DEEPGAN_224.hdf5", monitor='loss', verbose=1,
#                                  save_best_only=True, mode='auto', period=1)
# model = create_efficientnet()
# model.load_weights("EFN_DEEPGAN_224.hdf5")
# model.compile(optimizer=tf.keras.optimizers.Adam(lr=0.00000001),
#                   loss='sparse_categorical_crossentropy',
#                   metrics=['accuracy'])
# history = model.fit(X[1:1500], yprim[1:1500], epochs=10, validation_split=0.2, class_weight='auto'
#                         , callbacks=[checkpoint])
#
# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('model loss')
# plt.ylabel('loss')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.show()


# model_new.summary()
# #
model_new = create_efficientnet()
model_new.load_weights("EFN_DEEPGAN_224.hdf5")
# model_new.summary()
# # model_new.load_weights(WEIGHTS)
#
#
# # model_new.evaluate(X[1800:2400], yprim[1800:2400])
# Y = model_new.predict(X)
#
# Y_map = list(map(max, Y))
#
# Y_argmax = (Y[:, 0] < Y[:, 1]).astype(np.int)
#
# print(Y[1:10])
# print(Y_map[1:10])
# print(Y_argmax[1:100])
#
# print(yprim[1:100])
#
# print(np.average(Y_argmax[1:2200] == yprim[1:2200]))
Y = model_new.predict(X)
Y_argmax = (Y[:, 0] < Y[:, 1]).astype(np.int)
print(confusion_matrix(yprim[1:2306], Y_argmax[1:2306]))



# model_new.summary()
