import os
import cv2
import numpy as np
from tqdm import tqdm
import random
import pickle

DATADIR = "C:/Users/lykan/Desktop/deepfake" #Sciezka do folderu z folderami zdjec
CATEGORIES = ["TestoweFoty","TestoweFoty"]  # 1 wartosc - folder z jednym typem zdjec 2-wartosc drugi typ...
PICKLEPATH = "pickle/TestoweFoty/"

training_data = []
IMG_SIZE = 224

for category in CATEGORIES:
    path = os.path.join(DATADIR, category)
    class_num = CATEGORIES.index(category)
    for img in tqdm(os.listdir(path)):
        if img == "desktop.ini":
            continue
        # print(img)
        img_array = cv2.imread(os.path.join(path, img), 1)  #add colors 3 surface
        new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
        training_data.append([new_array, class_num])




print((len(training_data)))

random.shuffle(training_data)

X = []
y = []

for features, label in training_data:
    X.append(features)
    y.append(label)

print(X[0].reshape(-1, IMG_SIZE, IMG_SIZE, 3).shape)
print(X[0].shape)


X = np.array(X).reshape(-1, IMG_SIZE, IMG_SIZE, 3)  #add colors 3 surface

print(len(X), len(y))

pickle_out = open(PICKLEPATH + "Test.pickle", "wb")
pickle.dump(X, pickle_out)
pickle_out.close()

pickle_out = open(PICKLEPATH + "Test.pickle", "wb")
pickle.dump(y, pickle_out)
pickle_out.close()